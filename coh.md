---
layout: page
theme: dark
title: Code Of Honor
permalink: /code-of-honor/
---

I always try to live by my code of honor. A man is nothing without his honor. "A sin is to betray your beliefs".

## Honorable life
* Don't betray your code of honor.
* Defend your Bushido way.
* Live life as a Honorable Man.
* Don't lie.
* Honesty takes courage.
* Don't talk without a knowledge to support it.
* "Don't argue with the crazy guy".
* Always follow your sense of Justice
* Feel the pain of others.
* Always put your self in people's shoe before making any decision.
* A simple smile goes a long way.
* There's no honor in winning by cheating.
* "If you fall down 7 times make sure to get up 8 times."
* Protect your promises.
* "For a samurai everywhere is Japan."
* Holding back is disrespectful to your opponent.
* Live life in your way.
* "Respect is earned, not given", So earn it.
* "A man is much more than the job he holds and clothes he wears."

## Science
> Truth is sought for its own sake … Finding the truth is difficult, and the road to it is rough.
> For the truths are plunged in obscurity … God, however, has not preserved the scientist from error
> and has not safeguarded science from shortcomings and faults. If this had been the case, scientists
> would not have disagreed upon any point of science… Therefore, the seeker after the truth is not one
> who studies the writings of the ancients and, following his natural disposition, puts his trust in them,
> but rather the one who suspects his faith in them and questions what he gathers from them, the one who
> submits to argument and demonstration, and not to the sayings of a human being whose nature is fraught
> with all kinds of imperfection and deficiency. Thus the duty of the man who investigates the writings
> of scientists, if learning the truth is his goal, is to make himself an enemy of all that he reads,
> and, applying his mind to the core and margins of its content, attack it from every side. He should
> also suspect himself as he performs his critical examination of it, so that he may avoid falling
> into either prejudice or leniency.

-- Ibn al-Haytham

## Programming
* Simplicity over Complexity
* Simple first, then Easy
* Abstraction, Abstraction, Abstraction
* Put your conventions layer on top of a well written abstraction
* Always choose a name for your variables which implies the variable content or usage.
* Never ever use variable names such as x, a, b, etc.
* Bugs before new features
* Docs before new features

### Library design
* Major releases can contain backward incompatible changes.
* It's better to use a different code name instead of a major version number change
  to indicate that there going to be backward incompatible changes.
* Minor releases shouldn't break the dependency versions. E.g: X(0.6.0) depends on Python(3.5.x),
  X(0.7.0) should depends on the same version python

### New Feature Checklist
* Is it easy to extend the feature ?
* Is it scalable ?
* Is it easy to maintain ?
* Is it well documented ?
* Does it have the best possible performance ?
* What about tests ?
* Is it following the correct coding style ?

### Resiliency Checklist

* Things that might fail:
  * Dependant systems
  * Network
  * External storage
  * Database
  * "The Cloud"

## Trading Checklist
* TBD

## Contribute to the Code Of Honor
If you found these codes useful and like to share your values with me, you're welcome to create a PR
on [the repository](https://gitlab.com/lxsameer/lxhome).
