---
layout: page
theme: dark
title: About Me
permalink: /about/

---

My name is Sameer aka lxsameer and I'm a software engineer and a proud member of [GNU project](https://gnu.org). As you may guessed by now
I'm a huge free software fan and really value freedome and privacy in the cyber world. If you're interested, we can exchange a word or two
via social networks (links on the header).
