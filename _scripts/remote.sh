#! /bin/bash

echo "[REMOTE]: Cleaning up..."
rm -rf ~/resources
echo "[REMOTE]: Extracting the package..."
tar zxf ~/tmp/package.tar.gz
echo "[REMOTE]: Installing the content..."
mkdir -p ~/lxsameer.com
cp -rv ~/_site/* ~/lxsameer.com/home/
chmod 755 ~/lxsameer.com/home/ -R
echo "[REMOTE]: Cleaning up..."
rm -rf ~/_site ~/tmp/
mkdir -p ~/tmp
echo "[REMOTE]: Done"
