---
layout: post
title:  "Sameer's Pyramid"
date:   2020-08-14
categories:
tags: technology rant
theme: dark
---
**TL,DR**: Don't follow trends in the world of technology because usually they are not based on scientific
reasons and only based on so many people who don't have enough insight talking about a subject.

During my day to day life I see many conversations on social media around trends in technology. People
try to settle many debates by resorting to trends. "Everyone is using it, so it has to be the best!",
"It's all over the web, so it has to be true" or "It's impossible for so many people to be wrong, so
it must be good" or tons of similar reasonings. I always wondered about the nature of trends and hypes
and how they navigate the world. This post is the summary of my thoughts on the topic that lead me to
a hypathesis on the behavior of trends and how to ride them.


By evolution, humans are social creatures. During the course of human history, we always tend to form
groups and communities, villages, cities and civilizations. We have found our comfort and safety living among others
and society. Trading with other, fighting along side others and exchange words and wisdom with other. Society gives us
confident and the sense of stability. Often, we seek validation in a group. Dealing we the unknown is out of our comfort
zone and plays with our minds and puts us in a doubtful state of mind. Humans by nature are againt change we don't want
to loose the sense of stability and safety by changes to our surroundings and being in doubt creates fear. In a situation
like this people tend to resort to a group of other people to validate their thoughts and get ride of the doubt that is
bugging them. Exchanging experience and thoughts helps us to think better and stay calm. Seeing people who
had the same experience gives us courage and helps us to push the fear away. Community validates our way of dealing with
the unknown in life. It gives us direction.

But in the history of mankind there were many people (but few in compare to total number of humans) who were adventurous
and overcame their fear of the unknown. //Instead of hiding within the worm and welcoming  and few people who breaks it . superstitios

In many cases people try to follow the hurd and the collective wisdom.
