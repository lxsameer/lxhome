---
layout: post
title:  "I moved from Github to Gitlab"
date:   2018-11-02
categories: Announcements
tags: gitlab
theme: dark
---
As soon as I hear about Micro$oft acquiring Github I decided to migrate to Gitlab. My guts tells me to do not trust a corporation behind the [Halloween document](https://en.wikipedia.org/wiki/Halloween_documents). Micro$oft might be a different company these days but their history is enough to not to trust them. Even if they are different and have no bad intentions, but still acquiring company such as Github means that the very first thing they will do is to integrate Github with their own services and that is enough for me to move.

In the other hand Gitlab is a free software with tons of feature like built-in CI and docker registry and other cool stuff. I totally recommend Gitlab as a better alternative to Github.
