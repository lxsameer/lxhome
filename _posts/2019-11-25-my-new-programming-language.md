---
layout: post
title:  "My new programming language"
date:   2019-11-25
categories: Programming
tags: Serene language
theme: dark
---
As a software engineer, one of my joys in life is to learn new things. I can't describe the
pleasure of learning a new technology or stepping forward in the world of science. If you experienced
such a delight, you would know that how addictive it is. I can't satisfy my hunger for knowledge and
it might sounds like a gloat but it's truly joyful (ok mate, you love to "read", get on with it).

Programming languages are the most common tool among programmers and software engineers (Duh!).
I believe that learning new programming languages helps us to widen our vision as engineers and
help us improve our mentality about software architecture and design. So clearly I never say "No"
to learning a new programming language and because of that I have lots of experience with different
languages. Each language taught me tons of new things and helped me enhance my skills. I've studied
many languages and have a long list of them as my "To Learn" list.

Approximately a year ago, I was frustrated with Python and nagging to my wife about it (She always listens to
my gibberish). All of a sudden she suggested to me that "Why don't you write your own programming language ?".
That got me thinking, "Is it a good idea to do so ????".

People have different routines for learning. I'm one of those people who likes to learn new
things by understanding how it works first. I'd like to start my learning process by understanding
the laws of the universe. In this case, "universe" is the implementation and theory behind
the thing I'm trying to learn. It might seem like crazy idea but that's how I learn better.
For instance, when I was a teenager and was learning about how to use Gnu/Linux, I was so
obsessed with internal of a Linux distribution to a degree that I decided to build my own
distribution. Of course as a teenager I was naive and dreamed about my distro ( Which I used
to call Liniera ) to become a well-known and popular distribution. Aside from my childhood dreams
I learned a lot by creating a distribution. Learned about Linux kernel, boot process, bootloaders
and tons of other complicated pieces that normally people don't get to know at first (I was using
LFS and Debian tools). So after that delightful but tough experience, I always try to build a minimal
prototype of whatever I'm trying to learn to comprehend the universe of that thing which
helped me a lot to this day.

Based on my history, routines and the question that my wife has planted in my mind and after about a year
researching and thinking about it, Finally I realized it can be a good idea to create a new
programming language. To be honest it is not a task to be taken lightly. Whenever I created something
that wasn't out of my needs, I just failed. But this time I think creating a programming language can
massively help me to gain a better grasp of "the universe". Trying to overcome this challenge will help
me to grow and be a better engineer despite the fact that this new language may not even make it to the
list of known programming languages. I'm fine with that as long as it pushes me a step forward in my way
of life and brings me joy of wondering around in the world of science and engineering.

I'm going to write more blog posts about my journey through this humongous task as a journal for myself and other
enthusiastic people. After all it will be a hobby of mine and not my day to day job. So I'll take my time and
move slowly but steady.
