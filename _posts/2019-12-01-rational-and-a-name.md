---
layout: post
title:  "Step 1: Rational (take one) and a name"
date:   2019-12-01
categories: Programming
tags: Serene language
theme: dark
---

**This post is a draft and I'll finish it gradually**

As I mentioned in [My new programming language](/programming/my-new-programming-language/),
I'm creating a new programming language. I'll try to pick up good points
of different programming languages and avoid the cons of them. One of the
most important aspects of any project is to have a rational for it.
It's what I learned from [Clojure](http://clojure.org)'s culture. Rationals
are a big deal in any clojure developers world. As fan I'd like to start
my new programming language by writing down the rational of what I'm trying
to achieve.

### Rational(Take 1) and goals
So far, the main reason to create a new language for me is to **learn more and
educate myself**. But it doesn't mean that I'm aiming for a toy language. I want
to create a general purpose language that solves some problems. Here is a list of
reasons that made me consider the idea of creating a new language (in no specific order):

#### Lisp is superior
I think the world needs more and more dialects of Lisp. It's the second oldest
programming language in the world and as far as I know the oldest one that is
still active. **Lisp** is elegant and amazing, but unfortunately not so many
programmers know about it. Even most of the those people who heard the name
are distracted by the "parenthesis". But they're missing the fact that there
is a good reason for all those parenthesis. Lisp is the simplest language I
know, its programs are written in its own datatypes. How simple is that???

You might hear that
[God has created the universe in Lisp](https://twobithistory.org/2018/10/14/lisp.html).
Lisp is amazing and I consider a programmer who has understood **the Lisp way**, the
[luckiest](https://twitter.com/lxsameer/status/1172220581992980480).

I'd my new language to be Lisp, because just being a Lisp brings a huge deal to the
table.

#### Simplicity
#### FP is the future
#### Development process
#### Better core development
#### Built-in Concurrency and parallelism

### A Name
If you're a programmer, I'm pretty sure that you already experienced the terror
of trying to find a name for your project. Frankly, It's even hard to find a good
name for your variable.

After about 10 days of searching finally my wife came up with a good name. **Serene**.
Calm and peaceful. I like it. It's simple and beautiful with a great meaning.

I can't wait to start working on it. :P
